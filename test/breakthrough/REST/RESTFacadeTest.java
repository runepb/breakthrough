package breakthrough.REST;
import breakthrough.Client.BreakthroughProxy;
import breakthrough.Server.InvokerImpl;
import breakthrough.TestDoubles.LocalMethodCallClientRequestHandler;
import breakthrough.domain.*;
import com.google.gson.Gson;
import frs.broker.ClientRequestHandler;
import frs.broker.Invoker;
import frs.broker.Requestor;
import frs.broker.marshall.json.StandardJSONRequestor;
import org.junit.*;

import javax.servlet.http.HttpServletResponse;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import java.io.*;

public class RESTFacadeTest {
    private RESTFacade facade;
    private String baseURL;
    private static final int port = 4567;
    private static final String hostname = "localhost";
    private Gson gson;
    @Before
    public void setUp(){
        facade = new RESTFacade();
        baseURL = "http://" + hostname + ":" + port + "/";
        gson = new Gson();
    }

    @Test
    public void shouldBeAbleToPostNewGame(){
        assertThat(facade.postOnPathGame(baseURL+"thisIsATestPath1/"),is(gson.toJson("1/")));
        assertThat(facade.getLastStatusCode(),is(HttpServletResponse.SC_CREATED));
        assertThat(facade.postOnPathGame(baseURL+"thisIsATestPath1/"),is(gson.toJson("2/")));
        assertThat(facade.getLastStatusCode(),is(HttpServletResponse.SC_CREATED));
    }

    @Test
    public void shouldBeAbleToMovePiece(){
        String id1 = facade.postOnPathGame(baseURL+"thisIsATestPath1/");
        Move move = new Move(new Position(6,1),new Position(5,1));
        String jsonMove = gson.toJson(move);
        String canMove = facade.putOnIdMove(""+1,jsonMove);
        assertThat(gson.fromJson(canMove,Boolean.class),is(true));
        assertThat(facade.getLastStatusCode(),is(HttpServletResponse.SC_ACCEPTED));
        Move move2 = new Move(new Position(6,1),new Position(5,1));
        String jsonMove2 = gson.toJson(move2);
        String canMove2 = facade.putOnIdMove(""+1,jsonMove2);
        assertThat(gson.fromJson(canMove2,Boolean.class),is(false));
        assertThat(facade.getLastStatusCode(),is(HttpServletResponse.SC_BAD_REQUEST));
    }

    @Test
    public void shouldBeAbleToGetGame(){
        String id1 =facade.postOnPathGame(baseURL+"thisIsATestPath1/");
        String game = facade.getFromIdGame(""+1);
        String gameTest= gson.toJson(new BreakthroughSurrogate());

        assertThat(game,is(gameTest));
        assertThat(facade.getLastStatusCode(),is(HttpServletResponse.SC_OK));
        String game2 = facade.getFromIdGame(""+3);
        assertThat(game2,is(nullValue()));
        assertThat(facade.getLastStatusCode(),is(HttpServletResponse.SC_NOT_FOUND));
    }
}
