package breakthrough.ClientAndServerTests;
import breakthrough.Server.InvokerImpl;
import breakthrough.TestDoubles.LocalMethodCallClientRequestHandler;
import breakthrough.common.OperationNames;
import breakthrough.domain.*;
import frs.broker.Requestor;
import frs.broker.marshall.json.StandardJSONRequestor;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
public class InvokerImplTest {
    Breakthrough game;
    InvokerImpl invokerImpl;
    LocalMethodCallClientRequestHandler crh;
    Requestor requestor;
    @Before
    public void setUp(){
        game = new BreakthroughSurrogate();
        invokerImpl = new InvokerImpl(game);
        crh = new LocalMethodCallClientRequestHandler(invokerImpl);
        requestor = new StandardJSONRequestor(crh);
    }

    @Test
    public void shouldParseGetPieceAtCorrectly(){
        Position p1 = new Position(1,1);
        Position p2 = new Position(6,1);
        Position p3 = new Position(4,1);


        Color reply1 = requestor.sendRequestAndAwaitReply("test", OperationNames.GET_PIECE_AT, Color.class, p1);
        assertThat(reply1,is(Color.BLACK));
        Color reply2 = requestor.sendRequestAndAwaitReply("test", OperationNames.GET_PIECE_AT, Color.class, p2);
        assertThat(reply2,is(Color.WHITE));
        Color reply3 = requestor.sendRequestAndAwaitReply("test", OperationNames.GET_PIECE_AT, Color.class, p3);
        assertThat(reply3,is(Color.NONE));
    }

    @Test
    public void shouldParseGetPlayerInTurnCorrectly(){
        Color reply1 = requestor.sendRequestAndAwaitReply("test", OperationNames.GET_PLAYER_IN_TURN, Color.class);
        assertThat(reply1,is(Color.WHITE));
    }

    @Test
    public void shouldParseGetWinnerCorrectly(){
        Color reply1 = requestor.sendRequestAndAwaitReply("test", OperationNames.GET_WINNER, Color.class);
        assertThat(reply1,is(Color.NONE));
    }

    @Test
    public void shouldParseMoveCorretly(){
        Position p1 = new Position(6,1);
        Position p2 = new Position(5,1);
        Move move = new Move(p1,p2);
        boolean reply1 = requestor.sendRequestAndAwaitReply("test", OperationNames.MOVE, boolean.class, move);
        assertThat(reply1,is(true));
        boolean reply2 = requestor.sendRequestAndAwaitReply("test", OperationNames.MOVE, boolean.class, move);
        assertThat(reply2,is(false));
    }
}
