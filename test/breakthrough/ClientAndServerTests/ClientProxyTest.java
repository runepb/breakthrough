package breakthrough.ClientAndServerTests;

import breakthrough.common.OperationNames;
import breakthrough.domain.*;
import breakthrough.Client.*;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class ClientProxyTest {
    Breakthrough game;
    SpyRequestor requestor;

    @Before
    public void setUp(){
        requestor = new SpyRequestor();
        game = new BreakthroughProxy(requestor);
    }

    @Test
    public void shouldReactCorrectlyToMoveCommand(){
        Move thisMove = new Move(new Position(6,2),new Position(5,2));
        game.move(thisMove);
        assertThat(requestor.lastOperationName,is(OperationNames.MOVE));
        assertThat(requestor.lastArgument[0],is(thisMove));
    }
    @Test
    public void shouldReactCorrectlyToGetPieceAtCommand(){
        Position lookingAtPosition = new Position(1,1);
        game.getPieceAt(lookingAtPosition);
        assertThat(requestor.lastOperationName,is(OperationNames.GET_PIECE_AT));
        assertThat(requestor.lastArgument[0],is(lookingAtPosition));
    }

    @Test
    public void shouldReactCorrectlyToGetPlayerInTurnCommand(){
        game.getPlayerInTurn();
        assertThat(requestor.lastOperationName,is(OperationNames.GET_PLAYER_IN_TURN));
    }

    @Test
    public void shouldReactCorrectlyToGetWinnerCommand(){
        game.getWinner();
        assertThat(requestor.lastOperationName,is(OperationNames.GET_WINNER));
    }
}
