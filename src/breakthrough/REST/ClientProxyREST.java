package breakthrough.REST;

import breakthrough.domain.*;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import frs.broker.IPCException;
import frs.broker.ipc.http.MimeMediaType;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

public class ClientProxyREST implements Breakthrough{

    private String baseURL;
    private Gson gson;
    private String gameId;
    public ClientProxyREST(String hostname,int port,String gameId){
        baseURL = "http://" + hostname + ":" + port + "/";
        gson = new Gson();
        this.gameId=gameId;
    }

    public ClientProxyREST(String hostname,int port){
        baseURL = "http://" + hostname + ":" + port + "/";
        gson = new Gson();
        HttpResponse<JsonNode> jsonResponse = null;
        try {
            jsonResponse = Unirest.post(baseURL + "Game/").
                    header("Accept", MimeMediaType.APPLICATION_JSON).
                    header("Content-Type", MimeMediaType.APPLICATION_JSON).
                    asJson();
        } catch (UnirestException e) { throw new IPCException("Game could not be created'", e);}
        String id = jsonResponse.getHeaders().getFirst("Location");
        gameId=id;
    }




    @Override
    public Color getPieceAt(Position p) {
        HttpResponse<JsonNode> jsonResponse = null;
        try {
            jsonResponse = Unirest.get(baseURL + "Game/"+ gameId).
                    header("Accept", MimeMediaType.APPLICATION_JSON).
                    header("Content-Type", MimeMediaType.APPLICATION_JSON).
                    asJson();
        } catch (UnirestException e) { throw new IPCException("Game could not be returned(getPieceAt)", e);}
        BreakthroughSurrogate game = gson.fromJson(jsonResponse.getBody().toString(), BreakthroughSurrogate.class);
        Color returnColor = game.getPieceAt(p);
        return returnColor;
    }

    @Override
    public Color getPlayerInTurn() {
        HttpResponse<JsonNode> jsonResponse = null;
        try {
            jsonResponse = Unirest.get(baseURL + "Game/" + gameId).
                    header("Accept", MimeMediaType.APPLICATION_JSON).
                    header("Content-Type", MimeMediaType.APPLICATION_JSON).
                    asJson();
        } catch (UnirestException e) { throw new IPCException("Game could not be returned(getPlayerInTurn)", e);}
        BreakthroughSurrogate game = gson.fromJson(jsonResponse.getBody().toString(), BreakthroughSurrogate.class);
        Color returnColor = game.getPlayerInTurn();
        return returnColor;
    }

    @Override
    public Color getWinner() {
        HttpResponse<JsonNode> jsonResponse = null;
        try {
            jsonResponse = Unirest.get(baseURL + "Game/" + gameId).
                    header("Accept", MimeMediaType.APPLICATION_JSON).
                    header("Content-Type", MimeMediaType.APPLICATION_JSON).
                    asJson();
        } catch (UnirestException e) { throw new IPCException("Game could not be returned(getWinner)", e);}
        BreakthroughSurrogate game = gson.fromJson(jsonResponse.getBody().toString(), BreakthroughSurrogate.class);
        Color returnColor = game.getWinner();
        return returnColor;
    }

    @Override
    public boolean move(Move move) {
        HttpResponse jsonResponse = null;
        String payloade = gson.toJson(move);
        try {
            jsonResponse = Unirest.put(baseURL + "Move/" + gameId).
                    header("Accept", MimeMediaType.APPLICATION_JSON).
                    header("Content-Type", MimeMediaType.APPLICATION_JSON).
                    body(payloade).asJson();
        } catch (UnirestException e) { throw new IPCException("Move could not be done", e);}
        boolean canMove = true;
        if (jsonResponse.getStatus() == HttpServletResponse.SC_BAD_REQUEST) canMove = false;
        return canMove;
    }

    public String getGameId(){
        return gameId;
    }
}