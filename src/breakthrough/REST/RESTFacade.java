package breakthrough.REST;

import breakthrough.Client.BreakthroughProxy;
import breakthrough.domain.*;
import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import frs.broker.IPCException;
import frs.broker.ipc.http.MimeMediaType;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

public class RESTFacade{
    private HashMap<String,Breakthrough> gameMap = new HashMap<>();
    private int id;
    private Gson gson;
    private int lastStatusCode;
    public RESTFacade(){
        id = 1;
        gson = new Gson();
    }
    public String postOnPathGame(String path) {
        Breakthrough game = new BreakthroughSurrogate();
        String stringId = "" + id++;
        gameMap.put(stringId,game);
        lastStatusCode = HttpServletResponse.SC_CREATED;
        return stringId;
    }
    public String putOnIdMove(String id, String jsonMove) {
        Move move = gson.fromJson(jsonMove,Move.class);
        Breakthrough game = gameMap.get(id);
        boolean canMove = game.move(move);
        if(canMove) lastStatusCode = HttpServletResponse.SC_OK;
        else lastStatusCode = HttpServletResponse.SC_BAD_REQUEST;
        return gson.toJson(canMove);
    }
    public String getFromIdGame(String id) {
        Breakthrough game = gameMap.get(id);
        String returnValue = gson.toJson(game);
        lastStatusCode = HttpServletResponse.SC_OK;
        if(game == null) {
            returnValue = null;
            lastStatusCode = HttpServletResponse.SC_NOT_FOUND;
        }
        return returnValue;
    }

    public int getLastStatusCode(){
        return lastStatusCode;
    }
}
