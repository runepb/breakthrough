package breakthrough.REST;

import static spark.Spark.*;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import breakthrough.domain.Breakthrough;
import breakthrough.domain.BreakthroughSurrogate;
import com.google.gson.*;

import frs.broker.*;
import frs.broker.ipc.http.MimeMediaType;

public class SparkJavaServerRequestHandlerAndInvokerImpl {
    private String lastVerb;
    private String baseURL;
    private  RESTFacade facade;
    private  int port;
    private  Gson gson;
    private int lastStatusCode;

    public SparkJavaServerRequestHandlerAndInvokerImpl(int portNumber, RESTFacade facade) {
        this.facade = facade;
        this.port = portNumber;
        gson = new Gson();
       // baseURL = "http://" + "localhost" + ":" + port + "/";
    }

    public void registerRoutes() {
// Set the port to listen to
        port(port);

        // POST = xxx
        String breakthroughRoute = "Game/";
        post("/"+breakthroughRoute, (req, res) -> {
            lastVerb = req.requestMethod();

            // Upcall to servant
            String id = facade.postOnPathGame("/" + breakthroughRoute);

            // Set the CREATED status code
            int statusCode = facade.getLastStatusCode();
            res.status(statusCode);
            res.type(MimeMediaType.APPLICATION_JSON);

            // Location = URL of created resource
            res.header("Location", id);

            lastStatusCode = statusCode;
            System.out.println(breakthroughRoute+id);
            System.out.println(statusCode);
            return "{}";
        });
        String moveRoute = "Move/"+":id";
        put("/"+moveRoute, (req, res) -> {
            lastVerb = req.requestMethod();
            String uniqueId = req.params(":id");
            // Demarshall the body into the move posted
            String body = req.body();
            facade.putOnIdMove(uniqueId,body);
            int statusCode = facade.getLastStatusCode();
            res.status(statusCode);
            res.type(MimeMediaType.APPLICATION_JSON);
            lastStatusCode = statusCode;
            System.out.println("Move/"+uniqueId);
            System.out.println(statusCode);
            System.out.println(body);
            return "{}";
        });


        String getRoute = "Game/"+":id";
        get("/"+getRoute, (req, res) -> {
            lastVerb = req.requestMethod();
            String uniqueId = req.params(":id");

            String game = facade.getFromIdGame(uniqueId);

            int statusCode = facade.getLastStatusCode();
            res.status(statusCode);
            res.type(MimeMediaType.APPLICATION_JSON);
            lastStatusCode = statusCode;
            System.out.println("Game/"+uniqueId);
            System.out.println(statusCode);
            return game;
        });
    }

    public void closedown() {
        stop();
    }

    public int lastStatusCode() {
        return lastStatusCode;
    }

    public String lastHTTPVerb() {
        return lastVerb;
    }

}

