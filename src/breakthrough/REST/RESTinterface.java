package breakthrough.REST;

public interface RESTinterface {
    public String postOnPathGame(String path);
    public String putOnPathMove(String path,String body);
    public String getFromIdGame(String id);
}
