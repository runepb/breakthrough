package breakthrough.Client;

import breakthrough.common.OperationNames;
import breakthrough.domain.Breakthrough;
import breakthrough.domain.Color;
import breakthrough.domain.Move;
import breakthrough.domain.Position;
import frs.broker.*;
import org.eclipse.jetty.http.MetaData;

public class BreakthroughProxy implements ClientProxy, Breakthrough {
    Requestor requestor;


    public BreakthroughProxy(Requestor requestor){
        this.requestor = requestor;
    }

    public BreakthroughProxy(){}

    @Override
    public Color getPieceAt(Position p) {
        return requestor.sendRequestAndAwaitReply("game100", OperationNames.GET_PIECE_AT, Color.class,p);
    }

    @Override
    public Color getPlayerInTurn() {
        return requestor.sendRequestAndAwaitReply("game100", OperationNames.GET_PLAYER_IN_TURN, Color.class);
    }

    @Override
    public Color getWinner() {
        return requestor.sendRequestAndAwaitReply("game100", OperationNames.GET_WINNER, Color.class);
    }

    @Override
    public boolean move(Move thisMove) {
        Boolean isValid = requestor.sendRequestAndAwaitReply("game100", OperationNames.MOVE, boolean.class,thisMove);
        if(isValid==null) return false;
        return isValid;
    }
}
