package breakthrough.common;
public class OperationNames {
  public static final String GET_PIECE_AT = "breakthrough-get-piece-at";
  public static final String GET_PLAYER_IN_TURN= "breakthrough-get-player-in-turn";
  public static final String GET_WINNER = "breakthrough-get-winner";
  public static final String MOVE = "breakthrough-move";
  public static final String GET_BOARD_STATE = "breakthrough-get-board-state";

}
