/**
  This package contains abstractions shared by client tier and
  application server tier.
*/
package breakthrough.common;
