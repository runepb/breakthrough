package breakthrough.main;

import breakthrough.REST.ClientProxyREST;
import breakthrough.domain.Breakthrough;
import breakthrough.ui.ClientInterpreter;

/** Client for breakthrough, using REST.
 */
public class ClientMainREST {
  public static void main(String args[]) {
    System.out.println("=== Client REST ===");

    String host = args[0];
    String op = args[1];
    String location = args[2];

    // REST proxy type
    ClientProxyREST game;

    if (op.equals("create")) {
      game = new ClientProxyREST(host,4567);
      location=game.getGameId();
    } else {
      game = new ClientProxyREST(host,4567,location);
    }
    System.out.println(" Op: " + op + ", Location: "+ location);
    
    // Start the interpreter
    ClientInterpreter interpreter =
        new ClientInterpreter(game,
                System.in, System.out);
    interpreter.readEvalLoop();

  }
}
