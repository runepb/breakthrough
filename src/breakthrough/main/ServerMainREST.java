package breakthrough.main;

import breakthrough.REST.RESTFacade;
import breakthrough.REST.SparkJavaServerRequestHandlerAndInvokerImpl;
import frs.broker.ServerRequestHandler;

/** App server for Breakthrough, using REST.
 */
public class ServerMainREST {
  
  public static void main(String[] args) throws Exception {
    new ServerMainREST(); // No error handling!
  }

  public ServerMainREST() throws Exception {
    int port = 4567;

    RESTFacade facade = new RESTFacade();
    SparkJavaServerRequestHandlerAndInvokerImpl SRH = new SparkJavaServerRequestHandlerAndInvokerImpl(port,facade);
    // and start it by registrering the routes to listen to
    SRH.registerRoutes();

    // Welcome
    System.out.println("=== Breakthrough REST (port:"+port+") ===");
    System.out.println(" Use ctrl-c to terminate!"); 


  }
}
