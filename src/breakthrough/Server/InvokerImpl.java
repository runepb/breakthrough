package breakthrough.Server;
import breakthrough.common.OperationNames;
import breakthrough.domain.Breakthrough;
import breakthrough.domain.Color;
import breakthrough.domain.Move;
import breakthrough.domain.Position;
import frs.broker.*;
import com.google.gson.*;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
public class InvokerImpl implements frs.broker.Invoker{
    private final Breakthrough game;
    private final Gson gson;
    public InvokerImpl(Breakthrough game){
        this.game=game;
        gson = new Gson();
    }

    @Override
    public ReplyObject handleRequest(String objectId, String operationName, String payload) {
        ReplyObject reply = null;
        JsonParser parser = new JsonParser();
        JsonArray array = parser.parse(payload).getAsJsonArray();

        if(operationName.equals(OperationNames.GET_PIECE_AT)){
            Position lookingAtPosition = gson.fromJson(array.get(0), Position.class);
            Color colorOfPiece = game.getPieceAt(lookingAtPosition);
            reply = new ReplyObject(HttpServletResponse.SC_OK,gson.toJson(colorOfPiece));
        }
        if(operationName.equals(OperationNames.GET_PLAYER_IN_TURN)){
            Color playerInTurn = game.getPlayerInTurn();
            reply = new ReplyObject(HttpServletResponse.SC_OK,gson.toJson(playerInTurn));
        }
        if(operationName.equals(OperationNames.GET_WINNER)){
            Color winner = game.getWinner();
            reply = new ReplyObject(HttpServletResponse.SC_OK,gson.toJson(winner));
        }
        if(operationName.equals(OperationNames.MOVE)){
            Move movement = gson.fromJson(array.get(0), Move.class);
            boolean isValid = game.move(movement);
            reply = new ReplyObject(HttpServletResponse.SC_OK,gson.toJson(isValid));
        }

    return reply;

    }
}
